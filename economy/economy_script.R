require(reshape2)

edu <- read.csv("economy.csv", header = T, na.strings = c("", ".."))
names(edu) <- sub(".*Country.Name$", "Country.Name", names(edu))


#Change colnames to just years for simplicity
colNames <- colnames(edu) 
years <- seq(1991,2015,1)
colnames(edu) <- c(colNames[1:4], years)

#stringify years
years <- as.character(years)


#Remove years with more than 4500 threshold
df <- data.frame(year=numeric(),numberOfNas=numeric())
years <- names(edu)[5:NCOL(edu)]
for (y in years) {
  df <- rbind(df, c(as.numeric(y), sum(is.na(edu[,c(y)]))))
}

plot(df, panel.first=grid())
colnames(df) <- c("Year", "NumberOfNa")
sum(df$NumberOfNa)

#number of allowed NAs in any column
threshold = 4000
yearsToKeep <- df[df$NumberOfNa < threshold,]$Year
edu <- edu[c(colNames[1:4], yearsToKeep)]

#remove NA rows and columns 
edu <- edu[rowSums(is.na(edu[,5:NCOL(edu)]))!=NCOL(edu)-4,]
edu <- edu[colSums(is.na(edu))<NROW(edu)]



# find countries with more then 100 features
t <- as.data.frame(table(edu$Country.Name))
t <- t[t$Freq > 115,]$Var1
edu <- edu[edu$Country.Name %in% t,]

# remove countries with missing features
ta <- table(edu$Country.Name, edu$Series.Name)
ta <- as.data.frame(ta)
cnames <- unique(ta[ta$Freq == 1,]$Var1)
edu <- edu[edu$Country.Name %in% cnames,]

# separate countries with most common features
t <- as.data.frame(table(edu$Series.Name))
maxFreq <- max(t$Freq)
series <- t[t$Freq == maxFreq,]$Var1
edu <- edu[edu$Series.Name %in% series,]


# FILLING THE GAPS
for(rowNumber in c(1:nrow(edu))){
  for (year in which(is.na(edu[rowNumber,]))){
    notNa<-setdiff(c(5:NCOL(edu)),which(is.na(edu[rowNumber,])))
    if(year==5)
      edu[rowNumber,year]<-edu[rowNumber,notNa[1]]
    else if(year==NCOL(edu))
      edu[rowNumber,year]<-edu[rowNumber,tail(notNa,n=1)]
    else{
      before<-0
      if(findInterval(year,notNa)==0) 
        before<-notNa[findInterval(year, notNa)+1]  
      else 
        before<-notNa[findInterval(year, notNa)]
      after<-notNa[findInterval(year, notNa)+1]
      if(is.na(after))after<-before
      beforeValue<-edu[rowNumber,before]
      afterValue<-edu[rowNumber,after]
      step<-0
      if(after==before) 
        step<-0
      else 
        step<-(afterValue-beforeValue)/(after-before)
      edu[rowNumber,year]<-beforeValue+(year-before)*step
    }
  }
}

# NORMALIZATION
normalize <- function (x) {
  (x-min(x, na.rm=T))/(max(x, na.rm=T)-min(x, na.rm=T))
}
indicators <- unique(edu$Series.Name)
for(i in indicators) {
  years <- names(edu)[5:NCOL(edu)]
  for(y in years) {
    edu[edu$Series.Name == i,][c(y)] <- normalize(edu[edu$Series.Name == i,][c(y)])
  }
}

#function for clustering 
clusterData <- function(years){
  for(y in years){
    t <- dcast(data = edu, formula = Country.Name ~ Series.Name, value.var = y)
    colNames <- colnames(t)
    colnames(t) <- c("Country.name", (paste(y, colNames[2:ncol(t)])))
    if(y == years[1]){
      dataForClustering <- t
    }
    else{
      dataForClustering <- merge(dataForClustering, t)
    }
  }
  dataForClustering
}

#plot clustered data
plotCluster<- function(dataForClustering,name){
  clustering.dist <- dist(dataForClustering)
  cluster <- hclust(clustering.dist)
  plot(cluster, labels = dataForClustering$Country.name, main=paste(name[1],"-",name[length(name)]))
}


#testing plot
plotCluster(clusterData(years[1:6]),years[1:6])


library(dendextend)
groupCodes <- c()
nordicCountries = c("Sweden", "Denmark", "Finland", "Norway")
dataForClustering<-clusterData(years)
for(name in dataForClustering$Country.name){
  if(name %in% nordicCountries){
    groupCodes <- c(groupCodes, "Nordic")
  }
  else if(name == "Estonia"){
    groupCodes <- c(groupCodes, "Estonia")
  }
  else{
    groupCodes <- c(groupCodes, "Normal")
  }
}

colorCodes <- c(Nordic="blue", Normal="black", Estonia="green")
#For making animations
for(y in years){
  t <- dcast(data = edu, formula = Country.Name ~ Series.Name, value.var = y)
  t.dist <- dist(t)
  t.cluster = hclust(t.dist)
  
  filename = paste("./images/",y,".png", sep="")
  #png settings
  png(file=filename,width     = 3.25,
      height    = 3.25,
      units     = "in",
      res       = 1200,
      pointsize = 4)
  
  dend <- as.dendrogram(t.cluster, hang=-1)
  labels_colors(dend) <- colorCodes[groupCodes][order.dendrogram(dend)]
  labels(dend) <- dataForClustering$Country.name[order.dendrogram(dend)]
  # Plotting the new dendrogram
  plot(dend)
  dev.off()
}